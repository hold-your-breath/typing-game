import { createElement } from "./helper.js";

let mainText;
let gameEnd = false;
let charIndex = 0;

export const runGame = (text, SECONDS_FOR_GAME) => {
  mainText = text;
  initializeGame(text, SECONDS_FOR_GAME);
  const counter = document.getElementById('counter');

  const timerId = setInterval(() => {
    counter.innerHTML = `${--SECONDS_FOR_GAME} seconds left`;
    if(gameEnd) {
      clearInterval(timerId);
      alert('Somebody end typing.');
    }
  }, 1000);

  setTimeout(() => {
    clearInterval(timerId);
    console.log('time out');
  }, SECONDS_FOR_GAME*1000);
};

const initializeGame = (text, SECONDS_FOR_GAME) => {
  const gameField = document.getElementById('game-field');
  const keyboardRace = createElement({
    tagName: 'div',
    className: '',
    attributes: { id: 'keyboardRace' }
  });
  createMainText(keyboardRace, text);
  createCounter(keyboardRace, SECONDS_FOR_GAME);

  gameField.append(keyboardRace);
};

const typing = (e) => {
  const typed = e.key;
  let letters = document.querySelectorAll('.letter');
  let letterHtml = letters[charIndex] ? letters[charIndex].innerHTML : undefined;
  let letterValue = letterHtml && letterHtml === '&nbsp;'
  ? ' '
  : letterHtml;

  if(typed === letterValue) {
    letters[charIndex].classList.add('bg');
    charIndex++;
  }

  if(letters.length === charIndex + 1){
    gameEnd = true;
  }
}

const createMainText = (keyboardRace, text) => {
  const mainText = createElement({
    tagName: 'div',
    className: 'main-text'
  });
  createWordArray(text, mainText);
  keyboardRace.append(mainText);
};

const createWordArray = (text, textBlock) => {
  const wordArray = text.split("");
  for(let i = 0; i< wordArray.length; i++) {
    const span = createElement({
      tagName: 'span',
      className: 'letter'
    });
    wordArray[i] === ' '
    ? span.innerHTML = `&nbsp;`
    : span.innerHTML = wordArray[i];
    textBlock.appendChild(span);
  }
};

const createCounter = (keyboardRace, SECONDS_FOR_GAME) => {
  const counter = createElement({
    tagName: 'div',
    className: '',
    attributes: {id:'counter'}
  });
  counter.innerHTML = `${SECONDS_FOR_GAME} seconds left`;
  keyboardRace.append(counter);
}

window.addEventListener('keydown', typing, false);
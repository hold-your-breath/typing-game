import { createElement } from './helper.js';

export const createUserItem = ({ position, name }) => {
  // position is number from 1 to 5
  const info = createUserInfo(name);
  const userItem = createElement({
    tagName: 'div',
    className: `user-item-${position}`
  });
  
  userItem.append(info);
  
  return userItem;
};

const createUserInfo = (name) => {
  const status = createUserStatus();
  const userName = createUserName(name);
  const statusBar = createuserStatusBar();
  const info = createElement({
    tagName: 'div',
    className: 'info'
  });

  info.append(status, userName, statusBar);

  return info;
};

const createUserStatus = () => {
  const status = createElement({
    tagName: 'div',
    className: 'status'
  });

  return status;
};

const createUserName = (name) => {
  const userName = createElement({
    tagName: 'div',
    className: 'name'
  });
  userName.innerText = name;

  return userName;
};

const createuserStatusBar = () => {
  const statusBar = createElement({
    tagName: 'div',
    className: 'status-bar'
  });

  return statusBar;
};
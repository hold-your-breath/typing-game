import { gameService } from '../services/gameService.js';
import {createUserItem} from './gameHelper.js';
import { runGame } from './keyboardRace.js';
import { runTimer } from './timer.js';

const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}
const socket = io("http://localhost:3002/game", { query: { username } });
const userItems = document.getElementById('user-items');
// const userInfoHeader = document.getElementById("username");
// userInfoHeader.innerHTML = `Hello, ${username}`;
const logoutButton = document.getElementById("logout");
const readyBtn = document.getElementById('readiness');

const onUsersReceive = ({ users, currentUser }) => {
  users.map((user, index) => {
    const userName = user.values.name;
    const userItem = userName === currentUser
    ? createUserItem({position: index+1, name: `${userName} (you)`})
    : createUserItem({position: index+1, name: userName});
    
    const hasChild = userItems.querySelector(`.user-item-${index+1}`);
    if(!hasChild) {
      userItems.appendChild(userItem);
    };
  });
};

const onLogOut = () => {
  socket.emit('logout', username);
  sessionStorage.removeItem("username");
  window.location.replace('/login');
};

const updateBtnText = (isReady) => {
  const readyBtn = document.getElementById('readiness');
  isReady
  ? readyBtn.innerText = 'NOT READY'
  : readyBtn.innerText = 'READY';
  console.log(readyBtn.innerText);
};

const updateStatusColor = (isReady, index) => {
  const status = document.querySelector(`.user-item-${index+1} .info .status`);
  isReady
  ? status.style.backgroundColor = 'green'
  : status.style.backgroundColor = 'red';
}

const canStartGame = users => users.every(user => user.values.isReady);

const onUsersReadiness = async (users) => {
  users.map((user, index) => {
    const isReady = user.values.isReady;
    updateStatusColor(isReady, index);
  });
  if(canStartGame(users)){
    runTimer(3, async () => {
      const random = Math.floor(Math.random() + 6);
      const { text } = await gameService.getSpecificText(random);
      runGame(text, 60);
    });
  }
};

const changeBtnText = user => {
  console.log(user.values.name);
  const isReady = user.values.isReady;
  updateBtnText(isReady);
};

const onReady = () => {
  socket.emit('toggle_user_status', username);
};

readyBtn.addEventListener('click', onReady);
logoutButton.addEventListener('click', onLogOut);
socket.on('receive_users', onUsersReceive);
socket.on('user_readiness', onUsersReadiness);
socket.on('change_button_text', changeBtnText);
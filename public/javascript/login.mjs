const socket = io('/login');
const username = sessionStorage.getItem("username");
const submitButton = document.getElementById("submit-button");
const input = document.getElementById("username-input");

const getInputValue = () => input.value;

const onClickSubmitButton = () => {
  const inputValue = getInputValue();
  if (!inputValue) {
    return;
  }
  sessionStorage.setItem("username", inputValue);
  login(inputValue);
};

const customizeUsernameStatus = (status, addClass, removeClass) => {
  // status can be: "free" | "used"
  // addClass and removeClass can be: "free" | "error"
  const usernameStatus = document.getElementById('username-status');
  usernameStatus.innerHTML = `Name is ${status}`;
  usernameStatus.classList.add(addClass);
  usernameStatus.classList.remove(removeClass);
}

const onInputName = () => {
  socket.emit('TYPING', input.value);

  socket.on('CHECK_NAME', isUsed => {
    isUsed
    ? customizeUsernameStatus('used', 'error', 'free')
    : customizeUsernameStatus('free', 'free', 'error');
  });
}

const onKeyUp = ev => {
  const enterKeyCode = 13;
  if (ev.keyCode === enterKeyCode) {
    submitButton.click();
  }
};

const login = name => {
  socket.emit('ADD_USER', name);
  socket.on('login', canLogIn => {
    canLogIn
    ? window.location.replace('/game')
    : alert('Username is already in use!');
  })
};

input.addEventListener("input", onInputName);
submitButton.addEventListener("click", onClickSubmitButton);
window.addEventListener("keyup", onKeyUp);
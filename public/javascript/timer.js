import { createElement } from './helper.js';
const SECOND_IN_MILLISECONDS = 1000;

export const runTimer = (seconds, onTimerEnd) => {
  const gameField = document.getElementById('game-field');
  initializeTimer(gameField);
  const timer = document.getElementById('timer');

  const timerId = setInterval(() => {
    timer.innerHTML = (--seconds);
  }, SECOND_IN_MILLISECONDS);

  setTimeout(() => {
    clearInterval(timerId);
    gameField.removeChild(timer);
    onTimerEnd();
  }, seconds * SECOND_IN_MILLISECONDS);
}

const initializeTimer = (gameField) => {
  const readyBtn = document.getElementById('readiness');
  const backBtn = document.getElementById('back-to-rooms');
  backBtn.style.display = 'none';
  readyBtn.style.display = 'none';
  createTimer(gameField);
};

const createTimer = (gameField) => {
  const timer = createElement({
    tagName: 'div',
    className: '',
    attributes: { id: 'timer' }
  });
  gameField.append(timer);
}
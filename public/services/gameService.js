import { callApi } from '../helpers/apiHelper.js';

class GameService {
  getSpecificText = async (id) => {
    try {
      const endpoint = `game/texts/${id}`;
      const apiResult = await callApi(endpoint, 'GET');
  
      return apiResult;
    } catch (error) {
      throw error;
    }
  } 
}

export const gameService = new GameService();
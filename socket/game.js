import { users } from "./login";

export default io => {
  io.on("connection", socket => {
    const userName = socket.handshake.query.username;
    const data = {users, currentUser: userName};

    socket.emit('receive_users', data);
    socket.broadcast.emit('receive_users', {users, currentUser: null});
    socket.emit('user_readiness', users);
    
    socket.on("logout", username => {
      const userIndex = users.findIndex(user => user.values.name === username);
      users.splice(userIndex, 1);
      socket.disconnect();
    });

    socket.on('toggle_user_status', username => {
      const user = users.find(user => user.values.name === username);
      const isReady = user.values.isReady
      user.values.isReady = !isReady;
      socket.emit('user_readiness', users);
      socket.broadcast.emit('user_readiness', users);
      socket.emit('change_button_text', user);
    });
    
    socket.on('disconnect', () => {
      console.log(`socket ${socket.id} disconnected.`);
    })
  });
};
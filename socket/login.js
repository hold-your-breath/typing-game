export let users = [];

const isUsedName = username => {
  const isUserUsed = users.find(user => user.values.name === username);
  return isUserUsed;
}

export default io => {
  io.on("connection", socket => {
    socket.on('ADD_USER', username => {
      const canLogIn = !isUsedName(username);
  
      if(canLogIn){
        users.push({ id: socket.id, values: { name: username, isReady: false } });
      }
      
      socket.emit('login', canLogIn);
    });
    
    socket.on('TYPING', inputValue => {
      const isUsed = isUsedName(inputValue);
      socket.emit('CHECK_NAME', isUsed);
    });
  });
};
